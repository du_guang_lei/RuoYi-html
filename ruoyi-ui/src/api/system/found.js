import request from '@/utils/request'

// 查询寻物启事列表
export function listfound(found) {
  return request({
    url: '/system/found/list',
    method: 'get',
    params: found
  })
}

// 查询寻物启事详细
export function getfound(foundId) {
  return request({
    url: '/system/found/' + foundId,
    method: 'get'
  })
}

// 新增寻物启事
export function addfound(data) {
  return request({
    url: '/system/found',
    method: 'post',
    data: data
  })
}

// 修改寻物启事
export function updatefound(data) {
  return request({
    url: '/system/found',
    method: 'put',
    data: data
  })
}

// 删除寻物启事
export function delfound(FoundId) {
  return request({
    url: '/system/found/' + FoundId,
    method: 'delete'
  })
}
