import request from '@/utils/request'

// 查询招聘求职列表
export function listoffer(offer) {
  return request({
    url: '/system/offer/list',
    method: 'get',
    params: offer
  })
}

// 查询招聘求职详细
export function getoffer(offerId) {
  return request({
    url: '/system/offer/' + offerId,
    method: 'get'
  })
}

// 新增招聘求职
export function addoffer(data) {
  return request({
    url: '/system/offer',
    method: 'post',
    data: data
  })
}

// 修改招聘求职
export function updateoffer(data) {
  return request({
    url: '/system/offer',
    method: 'put',
    data: data
  })
}

// 删除招聘求职
export function deloffer(offerId) {
  return request({
    url: '/system/offer/' + offerId,
    method: 'delete'
  })
}
